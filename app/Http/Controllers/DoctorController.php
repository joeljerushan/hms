<?php

namespace App\Http\Controllers;

use App\Doctor;
use App\DoctorApointments;
use App\DoctorAvailability;
use Illuminate\Http\Request;
use Carbon\Carbon;

class DoctorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $doctors = Doctor::get();
        return response()->json(['doctors' => $doctors], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function makeAppointment(Request $request)
    {
        //init query
        $appointment = new DoctorApointments;
        $appointment->token = $request->token;
        $appointment->patient_id = $request->patient_id;
        $appointment->doctor_id = $request->doctor_id;
        $appointment->room_id = $request->doctor_id;
        
        return 'asdad';
        //assigning saved user to a variable 
        $save_user = $user->save();

    }

    public function checkavailability(Request $request)
    { 
        
        $today = Carbon::now()->toDateString();

        $check_availability = DoctorAvailability::where('doctor_id', '=', $request->doctor_id)
                    ->where('created_at', '=', $today)->first();
        
        return $check_availability;

    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Doctor  $doctor
     * @return \Illuminate\Http\Response
     */
    public function show(Doctor $doctor)
    {
        if($doctor){
            return $doctor;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Doctor  $doctor
     * @return \Illuminate\Http\Response
     */
    public function edit(Doctor $doctor)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Doctor  $doctor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Doctor $doctor)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Doctor  $doctor
     * @return \Illuminate\Http\Response
     */
    public function destroy(Doctor $doctor)
    {
        //
    }
}
