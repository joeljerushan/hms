<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use App\Staff;
use App\Doctor;
use Illuminate\Http\Request;

//php artisan passport:client --personal

class UserAuthController extends Controller
{
    public function login(Request $request)
    {
        //get email and password from request
        $credentials = [
            'email' => $request->email,
            'password' => $request->password
        ];  

       
        if (auth()->attempt($credentials)) { //checking users table to authenticate
            //generate access token
            $token = auth()->user()->createToken('hms-app')->accessToken;
            
            //check unser_infos table where current user has record before login 
            //$Userinfo = UserInfo::where('user_id', '=', auth()->user()->id)->first();

            //check and get user from user table
            $get_update_user = User::where('id', '=', auth()->user()->id)->first();

            //return responses object to application
            return response()->json([
                'token' => $token, 
                'user' => $get_update_user,
            ], 200);
        } else {
            //if user not found show unauthorized error 
            return response()->json(['error' => 'Username or password incorrect.'], 401);
        }  
    }

    public function register(Request $request)
    {        
            //hashing password for security reason
            $password = bcrypt($request->password);

            //check incoming role
            $role = $request->role;
            $has_auth = $request->has_auth;

            if($role === 'doctor'){
                //get email address from request and check anyone has same email
                $isRegistered = User::where('email', '=', $request->email)->first();

                if($isRegistered) {
                    //if request email address already exists respond this error message
                    return response()->json(['error' => 'email already registered' ], 401); 
                } else { 
                    //init query
                    $user = new User;
                    $user->name = $request->name;
                    $user->email = $request->email;
                    $user->password = $password;
                    $user->role = $request->role;
                
                    //assigning saved user to a variable 
                    $save_user = $user->save();

                    //if user saved boolean true
                    if($save_user) {

                        //check and get user from user table
                        $get_update_user = User::where('id', '=', $user->id)->first();

                        //init doctor saving query
                        $doctor = new Doctor;
                        $doctor->user_id = $user->id;
                        $doctor->name = $request->name;
                        $doctor->nic = $request->nic;
                        $doctor->gender = $request->gender;
                        $doctor->address = $request->address;
                        $doctor->date_of_birth = $request->date_of_birth;
                        $doctor->contact = $request->contact;
                        $doctor->unit_type = $request->unit_type;
                        $doctor->category = $request->category;
                        $doctor->qualification = $request->qualification;
                        $doctor->bank = $request->bank;
                        $doctor->account_number = $request->account_number;
                        $doctor->fee = $request->fee;
                        $save_doctor = $doctor->save();

                        //if doctor save available
                        if($save_doctor) {
                            //get first doctor
                            $get_update_doctor = Doctor::where('user_id', '=', $user->id)->first();

                            //responding registered user object
                            return response()->json([ 'success' => 'Doctor saved Successfully with authentication access.' ], 200);   
                        } else {
                            //show error message if anythig breaks 
                            return response()->json(['error' => 'Error while saving doctor information.'], 401);
                        }                        
                    } else {
                            //show error message if anythig breaks 
                            return response()->json(['error' => 'Error while saving doctor authentication info'], 401);
                    } 
                }
            } else {
                if($has_auth === '1'){

                    //get email address from request and check anyone has same email
                    $isRegistered = User::where('email', '=', $request->email)->first();

                    if($isRegistered) {
                        //if request email address already exists respond this error message
                        return response()->json(['error' => 'email already registered' ], 200); 
                    } else {
                        //init query
                        $user = new User;
                        $user->name = $request->name;
                        $user->email = $request->email;
                        $user->password = $password;
                        $user->role = $request->role;
                    
                        //assigning saved user to a variable 
                        $save_user = $user->save();

                        //if user saved boolean true
                        if($save_user) {

                            //check and get user from user table
                            $get_update_user = User::where('id', '=', $user->id)->first();

                            //init staff saving query
                            $staff = new Staff;
                            $staff->user_id = $user->id;
                            $staff->name = $request->name;
                            $staff->designation = $request->designation;
                            $staff->department = $request->department;
                            $staff->gender = $request->gender;
                            $staff->address = $request->address;
                            $staff->date_of_birth = $request->date_of_birth;
                            $staff->contact = $request->contact;
                            $staff->salary = $request->salary;
                            $save_staff = $staff->save();

                            //if staff save available
                            if($save_staff) {
                                //get first staff
                                $get_update_staff = Staff::where('user_id', '=', $user->id)->first();

                                //responding registered user object
                                return response()->json([ 'success' => 'Staff saved Successfully with authentication access.' ], 200);   
                            } else {
                                //show error message if anythig breaks 
                                return response()->json(['error' => 'Please check again later'], 401);
                            }                        
                        } else {
                                //show error message if anythig breaks 
                                return response()->json(['error' => 'Please check again later'], 401);
                        } 
                    }
                } else {
                    //initiate data
                    $staff = new Staff;
                    $staff->name = $request->name;
                    $staff->designation = $request->designation;
                    $staff->department = $request->department;
                    $staff->gender = $request->gender;
                    $staff->address = $request->address;
                    $staff->date_of_birth = $request->date_of_birth;
                    $staff->contact = $request->contact;
                    $staff->salary = $request->salary;

                    //save 
                    $save_staff = $staff->save();
            
                    //if saved 
                    if($save_staff) { 
                        //respond success message
                        return response()->json([ 'success' => 'Staff saved Successfully' ], 200);  
                    } else {
                        //respond error message
                        return response()->json([ 'error' => 'Please fill all required data.' ], 401);
                    }
                }
            }
        
           
    }

    public function index()
    {
        $users = User::get();
        return response()->json(['users' => $users], 200);
    }

}
