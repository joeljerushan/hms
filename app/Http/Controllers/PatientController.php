<?php

namespace App\Http\Controllers;

use App\Patient;
use Illuminate\Http\Request;

class PatientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = Patient::get();
        return response()->json(['patient' => $list], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //init query
        $patient = new Patient;
        $patient->name = $request->name;
        $patient->nic = $request->nic;
        $patient->gender = $request->gender;
        $patient->address = $request->address;
        $patient->date_of_birth = $request->date_of_birth;
        $patient->age = $request->age;
        $patient->contact = $request->contact;
        $patient->guardian = $request->guardian;

        //assigning saved user to a variable 
        $save_patient = $patient->save();

        if($save_patient) { 
            return response()->json([ 'success' => 'true' ], 200); 
        } else {
            return response()->json(['error' => 'Error while saving patient'], 401);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Patient  $patient
     * @return \Illuminate\Http\Response
     */
    public function show(Patient $patient)
    {
        if($patient){
            return $patient;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Patient  $patient
     * @return \Illuminate\Http\Response
     */
    public function edit(Patient $patient)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Patient  $patient
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Patient $patient)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Patient  $patient
     * @return \Illuminate\Http\Response
     */
    public function destroy(Patient $patient)
    {
        //
    }

    public function patientByArea()
    {
        $batticaloa = Patient::where('address', '=', 'Batticaloa')->get();
        $ampara = Patient::where('address', '=', 'Ampara')->get();
        $chenkalady = Patient::where('address', '=', 'Chenkalady')->get();
        $kalmunai = Patient::where('address', '=', 'Kalmunai')->get(); 
        
        return response()->json([
            'batticaloa' => $batticaloa,
            'ampara' => $ampara,
            'chenkalady' => $chenkalady,
            'kalmunai' => $kalmunai,
        ], 200); 
    }

    public function patientByGender()
    {
        $male = Patient::where('gender', '=', 'Male')->get();
        $female = Patient::where('gender', '=', 'Female')->get();
        
        return response()->json([
            'male' => $male, //array of males 
            'male_count' => $male->count(), //total number of males
            'female' => $female, //array of females 
            'female_count' => $female->count(), //total number of female
        ], 200); 
    }

    public function getAppointmentsByDoctor(Request $request)
    {
        //get doctor id from request
        $doctor_id = $request->doctor_id;

        //get appointments by doctor id
        $appointments = Appointments::where('doctor_id', '=', $doctor_id)->get();

        //return total number appointments
        return response()->json([
            'appointments_count' => $appointments->count(), 
        ], 200); 
    }

    public function getDrugsSummaryCount(Request $request)
    {
        //get drug id from request
        $drug_id = $request->drug_id;

        //get all drugs in array
        $drugs = Drugs::get();

        //init empty array
        $list = [];

        foreach ($drugs as $key => $single) {
            //get single drug details 
            $single_drug = Drug::where('id', '=', $single->id)->get();

            //push it to init array $list
            //array_push($list, { name: $single->name, count: $single_drug->count() })
        }

        //return all
        return response()->json([
            'drugs' => $list, 
        ], 200); 
    }

    public function getIncomeSummary(Request $request)
    {
        $radiology_charge = Payments::select('radiology', Payments::raw('SUM(amount)'))->get();
        $pharmacy = Payments::select('pharmacy', Payments::raw('SUM(amount)'))->get();
        $lab_payment = Payments::select('lab', Payments::raw('SUM(amount)'))->get();
        $appointment = Payments::select('appointment', Payments::raw('SUM(amount)'))->get();
        
        return response()->json([
            'radiology' => $radiology_charge,
            'pharmacy' => $pharmacy,
            'lab' => $lab_payment,
            'appointment' => $appointment,
        ], 200); 
    }

    public function getIncomeSummaryByTime(Request $request)
    {
        //get request type 
        $type = $request->type;

        //get start time ie. 2019-12-13
        $start_time = $request->start_time;

        //get end time ie. 2019-12-12
        $end_time = $request->end_time;

        $charges = Payments::select($type, Payments::raw('SUM(amount)'))
                    ->whereBetween('date', [$start_time, $end_time])->get();
        
        return response()->json($charges, 200); 
    }

    public function getExpenseSummary(Request $request)
    {
        $purchases = Expenses::select('purchases', Expenses::raw('SUM(amount)'))->get();
        $salary = Expenses::select('salary', Payments::raw('SUM(amount)'))->get();
        $doctor_payment = Expenses::select('doctor_payment', Payments::raw('SUM(amount)'))->get();
        $others = Expenses::select('others', Payments::raw('SUM(amount)'))->get();
        
        return response()->json([
            'purchases' => $purchases,
            'salary' => $salary,
            'doctor_payment' => $doctor_payment,
            'others' => $others,
        ], 200); 
    }

    public function getExpenseSummaryByTime(Request $request)
    {
        //get expense request type 
        $type = $request->type;

        //get start time ie. 2019-12-13
        $start_time = $request->start_time;

        //get end time ie. 2019-12-12
        $end_time = $request->end_time;

        $expense = Expenses::select($type, Payments::raw('SUM(amount)'))
                    ->whereBetween('date', [$start_time, $end_time])->get();
        
        return response()->json($expense, 200); 
    }
}
