<?php

namespace App\Http\Controllers;

use App\DoctorApointments;
use Illuminate\Http\Request;

class DoctorApointmentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DoctorApointments  $doctorApointments
     * @return \Illuminate\Http\Response
     */
    public function show(DoctorApointments $doctorApointments)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DoctorApointments  $doctorApointments
     * @return \Illuminate\Http\Response
     */
    public function edit(DoctorApointments $doctorApointments)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DoctorApointments  $doctorApointments
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DoctorApointments $doctorApointments)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DoctorApointments  $doctorApointments
     * @return \Illuminate\Http\Response
     */
    public function destroy(DoctorApointments $doctorApointments)
    {
        //
    }
}
