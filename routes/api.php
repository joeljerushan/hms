<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/login', 'UserAuthController@login');
Route::post('/register', 'UserAuthController@register');
Route::get('/users/list', 'UserAuthController@index');

Route::get('/doctors/list', 'DoctorController@index');
Route::get('/doctors/get/{doctor}', 'DoctorController@show');
Route::post('/doctors/appointment/check', 'DoctorController@checkavailability');
Route::post('/doctors/appointment/create', 'DoctorController@makeAppointment');

Route::get('/staff/list', 'StaffController@index');
Route::get('/staff/get/{staff}', 'StaffController@show');

Route::post('/patient/add', 'PatientController@store');
Route::get('/patient/list', 'PatientController@index');
Route::get('/patient/get/{patient}', 'PatientController@show');

Route::post('/room/add', 'RoomsController@store');
Route::post('/room/get', 'RoomsController@show');


Route::get('/report/patient-by-address', 'PatientController@patientByArea');