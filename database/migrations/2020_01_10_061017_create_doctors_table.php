<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDoctorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doctors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->default(0);
            $table->string('name');
            $table->string('nic');
            $table->string('gender');
            $table->string('address');
            $table->dateTime('date_of_birth');
            $table->bigInteger('contact');
            $table->string('unit_type');
            $table->string('category');
            $table->string('qualification');
            $table->string('bank');
            $table->string('account_number');
            $table->boolean('status')->default(1);
            $table->bigInteger('fee');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doctors');
    }
}
